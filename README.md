# Ruby on Rails

Following the _Ruby On Rails - Getting started_ tutorial, which can be found [here](https://guides.rubyonrails.org/getting_started.html)

I have completed the project to the section _7.2 Resourceful Routing_. More to come...

## Environement and installation

Currently using Windows 11 (Planning to change to Linux in the future), Ruby can be installed using the Exe [here](https://rubyinstaller.org/).
After that, _rails_ must be installed with the following command:

```batch
gem install rails
```

The project is created with:

```batch
rails new <project-name>
```

To obtain hot reload features follow [this procedure](https://dev.to/thomasvanholder/how-to-set-up-rails-hotwire-live-reload-38i9) (For now, step 3 has been ignored).

When running the development server, a timeout error from Redis may occur. To eliminate that, [install redis on WSL2](https://redis.io/docs/getting-started/installation/install-redis-on-windows/) and start the server from there.
